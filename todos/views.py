from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView
from todos.models import TodoItem, TodoList


class TodoListView(ListView):
    model = TodoList
    template_name = "list/list.html"
    context_object_name = "todo_lists"


class TodoListDetailView(DetailView):
    model = TodoList
    template_name = "list/detail.html"
    context_object_name = "todo_list"


class TodoListCreateView(CreateView):
    model = TodoList
    template_name = "list/create.html"
    fields = ["name"]
    success_url = reverse_lazy("todo_list")
